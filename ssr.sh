#!/bin/bash
echo "------------------------------------------------------"
echo "|    Welcome to Murmur CHINA SSR installing tool     |"
echo "|       https://gitlab.com/murmurcn/ssr_script       |"
echo "------------------------------------------------------"
echo "|         Please prepare the following info          |"
echo "| *URL prefix of SSR server                          |"
echo "| *DDNS username & password                          |"
echo "| *SSR node id                                       |"
echo "------------------------------------------------------"
echo -n "Press any key to continue!"
read -s press
echo ""
# Ubuntu stuff
echo "Checking your machine type..."
uname="`uname -a | grep Ubuntu`"
kernel_q="n"
if [[ $uname =~ .*Ubuntu.* ]]; then
	kernel_ver="`uname -r`"
	if [[ ! $kernel_ver =~ Ubuntu ]]; then 
		echo "This is a Ubuntu machine. Your kernel is $kernel_ver."
		echo -n "Do you want to update your kernel to 5.3.0? (Y/n) "; read kernel_q
		if [ -z $kernel_q ]; then
			kernel_q="y"
		fi
		echo "Saved."
	fi
fi
echo "------------------------------------------------------"
echo "|            Setting your machine now...             |"
echo "------------------------------------------------------"
# install lastest kernel image (5.4.14)
if [[ $kernel_q =~ ^Y$|^y$ ]]; then
	echo "------------------------------------------------------"
	echo "|                  Updating Kernel                   |"
	echo "------------------------------------------------------"
	mkdir kernel && cd ./kernel
	wget https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.4.14/linux-headers-5.4.14-050414_5.4.14-050414.202001230832_all.deb
	wget https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.4.14/linux-headers-5.4.14-050414-generic_5.4.14-050414.202001230832_amd64.deb
	wget https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.4.14/linux-image-unsigned-5.4.14-050414-generic_5.4.14-050414.202001230832_amd64.deb
	wget https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.4.14/linux-modules-5.4.14-050414-generic_5.4.14-050414.202001230832_amd64.deb
	dpkg -i *.deb
	echo "------------------------------------------------------"
	echo "|                  Update finished                   |"
	echo "------------------------------------------------------"
	echo -n "Press any key to reboot your machine..."; read -s press
	reboot now
fi

# Question time!
echo "We gonna ask you some questines..."
main_while="n"
while [[ ! $main_while =~ ^Y$|^y$  ]]; do


	# SSR node ID
	echo -n "SSR NODE_ID = "; read node_id
	echo "Saved."
	# iptables
	echo -n "Do you want to adjust iptables? (y/N) "; read iptables_q
	if [ -z $iptables_q ]; then
		iptables_q="n"
	fi
	echo "Saved."
	# timezone & locale-gen
	echo "What timezone you wanna use? (def: Asia/Taipei) "; read timezone
	if [ -z $timezone ]; then
		timezone="Asia/Taipei"
	fi
	echo "Saved."
	# swap
	echo -n "Do you want to create swap (Y/n) "; read swap_q
	if [ -z $swap_q ]; then
		swap_q="y"
	fi
	echo "Saved."
	# bbr
	echo "Do you want to enbale bbr boost up? (Y/n)"
	echo "Your kernel version is: `uname -r`"
	echo "Warning: kernel version should be > 4.9"
	read bbr_q
	if [ -z $bbr_q ]; then
		bbr_q="y"
	fi
	echo "Saved."
	# ddclient
	echo -n "Do you want to install ddclient? (Y/n) "; read ddclient_q
	if [ -z $ddclient_q ]; then
		ddclient_q="y"
	fi
	if [[ $ddclient_q =~ ^Y$|^y$  ]]; then
		d_detail_q="n"
		while [[ ! $d_detail_q =~ ^Y$|^y$  ]]; do
			echo -n "Title of dns: "; read dd_url
			echo -n "Username: "; read dd_user
			echo -n "Password: "; read dd_psw
			echo ""
			echo "-----Here is your input------"
			echo "URL: $dd_url.d2.mailedu.pw"; echo "Username: $dd_user"; echo "Password: $dd_psw"
			echo -n "Correct? (y=n) "; read d_detail_ok
			if [[ $d_detail_ok =~ ^Y$|^y$  ]]; then
				d_detail_q="y"
			fi
		done
	fi
	# list all variables
	echo "------------------------------------------------------"
	echo "|             Here are all your settings             |"
	echo "------------------------------------------------------"
	echo "SSR NODE_ID: $node_id"
	echo "Adjust iptables: $iptables_q"
	echo "Timezone: $timezone"
	echo "Create swap: $swap_q"
	echo "Enable bbr: $bbr_q"
	echo "Install ddclient: $ddclient_q"
	if [[ $ddclient_q =~ ^Y$|^y$  ]]; then
		echo "URL: $dd_url.d2.mailedu.pw"; echo "Username: $dd_user"; echo "Password: $dd_psw"
	fi
	echo "------------------------------------------------------"
	echo "|                  Satisfied? (Y/n)                  |"
	echo "------------------------------------------------------"
	read main_while
	if [ -z $main_while ]; then
		main_while="y"
	fi 
done

echo "------------------------------------------------------"
echo "|                   updating apt                     |"
echo "------------------------------------------------------"
apt update && apt upgrade -y
echo "------------------------------------------------------"
echo "|               apt update finished                  |"
echo "------------------------------------------------------"

#if server is CHT, adjust firewall
if [[ $iptables_q =~ ^Y$|^y$ ]]; then
	echo "------------------------------------------------------"
	echo "|               adjusting iptables                   |"
	echo "------------------------------------------------------"
	/sbin/iptables -I INPUT -i eth0 -p tcp --dport 100:300 -j ACCEPT
	/sbin/iptables -I INPUT -i eth0 -p udp --dport 100:300 -j ACCEPT
	/sbin/iptables -I INPUT -i eth0 -p tcp --dport 100:300 -j ACCEPT
	/sbin/iptables -I INPUT -i eth0 -p udp --dport 100:300 -j ACCEPT
	mkdir $HOME/.ssr_iptables/
	ssr_iptables="$HOME/.ssr_iptables/ssr_iptables.sh"
	touch $ssr_iptables
	echo "PATH=/usr/sbin:/sbin:/usr/bin:/bin" >> $ssr_iptables
	echo "/sbin/iptables -I INPUT -i eth0 -p tcp --dport 100:300 -j ACCEPT" >> $ssr_iptables
	echo "/sbin/iptables -I INPUT -i eth0 -p udp --dport 100:300 -j ACCEPT" >> $ssr_iptables
	echo "/sbin/iptables -I INPUT -i eth0 -p tcp --dport 100:300 -j ACCEPT" >> $ssr_iptables
	echo "/sbin/iptables -I INPUT -i eth0 -p udp --dport 100:300 -j ACCEPT" >> $ssr_iptables
	chmod u+x $ssr_iptables
	(crontab -l 2>/dev/null; echo "@reboot $ssr_iptables") | crontab -
	echo "------------------------------------------------------"
	echo "|                iptables adjusted                   |"
	echo "------------------------------------------------------" 
fi

echo "------------------------------------------------------"
echo "|                 writing crontab                    |"
echo "------------------------------------------------------"
(crontab -l 2>/dev/null; echo "30 3 * * * /sbin/shutdown -r now") | crontab -
echo "------------------------------------------------------"
echo "|                  crontab writed                    |"
echo "------------------------------------------------------"

echo "------------------------------------------------------"
echo "|                setting timezone                    |"
echo "------------------------------------------------------"
apt install dbus locales -y
timedatectl set-timezone $timezone
timedatectl
locale-gen en_AU en_AU.UTF-8
echo "------------------------------------------------------"
echo "|                timezone writted                    |"
echo "------------------------------------------------------"

#create swap
if [[ $swap_q =~ ^Y$|^y$  ]]; then
	echo "------------------------------------------------------"
	echo "|                  creating swap                     |"
	echo "------------------------------------------------------"
	dd if=/dev/zero of=/var/swap bs=1024 count=1242880
	chmod 600 /var/swap
	mkswap /var/swap
	swapon /var/swap
	free
	echo "------------------------------------------------------"
	echo "|                  swap created                      |"
	echo "------------------------------------------------------"
fi

# adjust hosts.allow
echo "------------------------------------------------------"
echo "|              adjusting hosts.allow                 |"
echo "------------------------------------------------------"
cp /etc/hosts.allow /etc/hosts.allow.BAK
echo "ALL: ALL" >> /etc/hosts.allow
echo "------------------------------------------------------"
echo "|               hosts.allow adusted                  |"
echo "------------------------------------------------------"

# adjust fail2ban
echo "------------------------------------------------------"
echo "|                adjusting fail2ban                  |"
echo "------------------------------------------------------"
apt install fail2ban dnsutils -y
jail="/etc/fail2ban/jail.local"
mv $jail /dev/null
touch $jail
echo "[DEFAULT]" >> $jail 
echo "ignoreip = 127.0.0.0/8 10.0.0.0/8 172.16.0.0/12 192.168.0.0/16 114.34.73.105 35.244.127.64" >> $jail
echo "[sshd]" >> $jail
echo "enabled = true" >> $jail
echo "port = ssh" >> $jail
echo "filter = sshd" >> $jail
echo "logpath = /var/log/auth.log" >> $jail
echo "maxretry = 15" >> $jail
echo "findtime = 600" >> $jail 
echo "bantime = 3600" >> $jail
echo 'action_ = %(banaction)s[name=%(__name__)s, bantime="%(bantime)s", port="%(port)s", protocol="%(protocol)s", chain="%(chain)s"]' >> $jail
echo 'action_mw = %(banaction)s[name=%(__name__)s, bantime="%(bantime)s", port="%(port)s", protocol="%(protocol)s", chain="%(chain)s"]%(mta)s-whois[name=%(__name__)s, dest="%(destemail)s", protocol="%(protocol)s", chain="%(chain)s"]' >> $jail
echo 'action_mwl = %(banaction)s[name=%(__name__)s, bantime="%(bantime)s", port="%(port)s", protocol="%(protocol)s", chain="%(chain)s"]%(mta)s-whois-lines[name=%(__name__)s, dest="%(destemail)s", logpath=%(logpath)s, chain="%(chain)s"]' >> $jail
echo "action = %(action_)s" >> $jail
echo "------------------------------------------------------"
echo "|                 fail2ban adjusted                  |"
echo "------------------------------------------------------"

# BBR
if [[ $bbr_q =~ ^Y$|^y$  ]]; then
	echo "------------------------------------------------------"
	echo "|                   adjusting BBR                    |"
	echo "------------------------------------------------------"
	modprobe tcp_bbr
	echo "tcp_bbr" >> /etc/modules-load.d/modules.conf
	echo "net.core.default_qdisc=fq" >> /etc/sysctl.conf
	echo "net.ipv4.tcp_congestion_control=bbr" >> /etc/sysctl.conf
	sysctl -p
	sysctl net.ipv4.tcp_available_congestion_control
	sysctl net.ipv4.tcp_congestion.control
	sysctl --system
	echo "------------------------------------------------------"
	echo "|                   BBR adjusted                     |"
	echo "------------------------------------------------------"
fi

# DDCLIENT
if [[ $ddclient_q =~ ^Y$|^y$ ]]; then
	echo "------------------------------------------------------"
	echo "|                 setting ddclient                   |"
	echo "------------------------------------------------------"
	DEBIAN_FRONTEND=noninteractive apt -yq install ddclient
	dd_path="/etc/ddclient.conf"
	cp /dev/null /etc/ddclient.conf
	touch $dd_path
	echo "protocol=dyndns2" >> $dd_path
	echo "use=web" >> $dd_path
	echo "server=domains.google.com" >> $dd_path
	echo "ssl=yes" >> $dd_path
	echo "login=$dd_user" >> $dd_path
	echo "password=$dd_psw" >> $dd_path
	echo "$dd_url.d2.mailedu.pw" >> $dd_path
	ddclient
	update-rc.d ddclient defaults
	echo "------------------------------------------------------"
	echo "|                  ddclient setted                   |"
	echo "------------------------------------------------------"
fi

# install package
echo "------------------------------------------------------"
echo "|                  installing SSR                    |"
echo "------------------------------------------------------"
apt install build-essential wget libssl1.1 -y
wget https://github.com/jedisct1/libsodium/releases/download/1.0.18-RELEASE/libsodium-1.0.18.tar.gz
tar xf libsodium-1.0.18.tar.gz && cd libsodium-1.0.18
./configure && make -j2 && make install
ldconfig

apt --fix-broken install
apt install python python-pip git -y
pip install cymysql==0.9.4

cd /var
wget https://murmurcn.com/ssr.tar
tar -xvf ssr.tar
cd shadowsocks
chmod +x *.sh
sed -i "s/^NODE_ID = 6$/NODE_ID = $node_id/" /var/shadowsocks/userapiconfig.py
cp /var/shadowsocks/userapiconfig.py /var/shadowsocks/apiconfig.py

ssr_ser="/etc/systemd/system/ssr.service"
touch $ssr_ser
echo "[Unit]" >> $ssr_ser
echo "Description=ShadowsocksR server" >> $ssr_ser
echo "After=syslog.target" >> $ssr_ser
echo "After=network.target" >> $ssr_ser
echo "" >> $ssr_ser
echo "[Service]" >> $ssr_ser
echo "LimitCORE=infinity" >> $ssr_ser
echo "LimitNOFILE=512000" >> $ssr_ser
echo "LimitNPROC=512000" >> $ssr_ser
echo "Type=simple" >> $ssr_ser
echo "WorkingDirectory=/var/shadowsocks" >> $ssr_ser
echo "ExecStart=/usr/bin/python /var/shadowsocks/server.py" >> $ssr_ser
echo "ExecReload=/bin/kill -s HUP \$MAINPID" >> $ssr_ser
echo "ExecStop=/bin/kill -s TERM \$MAINPID" >> $ssr_ser
echo "Restart=always" >> $ssr_ser
echo "" >> $ssr_ser
echo "[Install]" >> $ssr_ser
echo "WantedBy=multi-user.target" >> $ssr_ser
systemctl enable ssr.service && systemctl start ssr.service
service ssr status
apt autoremove -y
echo "------------------------------------------------------"
echo "|                  SSR installed                     |"
echo "------------------------------------------------------"
echo "------------------------------------------------------"
echo "|                checking fail2ban                   |"
echo "------------------------------------------------------"
cat /etc/fail2ban/jail.local
echo "ALL GOOD?? (Y/n)"; read jail_ok
if [ -z $jail_ok ]; then
	jail_ok="y"
fi
if [[ $jail_ok =~ ^Y$|^y$  ]]; then
	service fail2ban restart
	sleep 3
	fail2ban-client status
	echo "------------------------------------------------------"
	echo "|                fail2ban checked                    |"
	echo "------------------------------------------------------"
else
	rm $jail
	service fail2ban stop
	echo "------------------------------------------------------"
	echo "|              fail2ban rollbacked                   |"
	echo "------------------------------------------------------"
fi

echo "------------------------------------------------------"
echo "|          All done! See you next time :)            |"
echo "------------------------------------------------------"
echo "Press [any] to continue..."; read -s -N 1 key
exit 0